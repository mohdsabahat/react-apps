import { useState, useRef } from 'react';

interface GeolocationError {
  code: number;
  message: string;
}

interface GeolocationState {
  location: GeolocationPosition | null;
  error: GeolocationError | null;
  speed: number | null;
}

const initialGeoLocationState: GeolocationState = {
  location: null,
  error: null,
  speed: null,
};

const DEFAULT_DELAY_BETWEEN_REQUESTS = 2000; // in milliseconds

const useGeolocation = (options: PositionOptions = {}) => {
  const [state, setState] = useState<GeolocationState>(initialGeoLocationState);

    let lastPosition: GeolocationPosition | null = null;
    let lastTime: number | null = null;
    let intervalId = useRef<NodeJS.Timer | null>(null);

    const startTracking = () => {
        console.log('Starting tracking...');
        const onSuccess: any = (position: GeolocationPosition) => {
            const currentTime = new Date().getTime();
            if (lastPosition && lastTime) {
                const deltaTime = (currentTime - lastTime) / 1000; // in seconds
                const distance = calculateDistance(lastPosition, position); // in meters
                const speed = distance / deltaTime; // in meters per second
                setState({ location: position, error: null, speed });
            } else {
                setState({ location: position, error: null, speed: null });
            }
            
            lastPosition = position;
            lastTime = currentTime;
        };

        const onError = (error: GeolocationError) => {
            setState({ location: null, error, speed: null});
        };

        intervalId.current = setInterval(() => {
            console.log('Requesting location...');
            navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
        }, DEFAULT_DELAY_BETWEEN_REQUESTS);
        
    };

    const stopTracking = () => {
        if (intervalId.current !== null) {
            console.log('Stopping tracking...');
            clearInterval(intervalId.current);
            intervalId.current = null;
        }
    };


  return {state, startTracking, stopTracking};
};

function calculateDistance(
    point1: GeolocationPosition,
    point2: GeolocationPosition
  ): number {
    const R = 6371e3; // Earth radius in meters
    const φ1 = (point1.coords.latitude * Math.PI) / 180;
    const φ2 = (point2.coords.latitude * Math.PI) / 180;
    const Δφ = ((point2.coords.latitude - point1.coords.latitude) * Math.PI) / 180;
    const Δλ = ((point2.coords.longitude - point1.coords.longitude) * Math.PI) / 180;
  
    const a =
      Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  
    const distance = R * c; // Distance in meters
    return distance;
  }

export default useGeolocation;
