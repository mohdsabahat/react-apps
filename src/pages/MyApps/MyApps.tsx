import React from 'react'
import { Col, Row } from 'react-bootstrap';
import CustomCard from '../../components/CustomCard/CustomCard';

import { myAppList } from '../../utils/appList';
import { NavLink } from 'react-router-dom';

const MyApps: React.FC = () => {

  return (
    <>
    <Row className='mb-2'>
        <Col>
            <h1>List of My Apps</h1>
        </Col>
    </Row>
    <Row className='gy-3' style={{paddingBottom: '46px'}}>
        {myAppList.length === 0 && <p>No apps found</p>}
        {myAppList.map((app, index) => (
            <Col xs={12} sm={6} md={4} lg={3} key={index}>
                <CustomCard title={app.name} cardText={app.description}>
                    <hr/>
                    <NavLink to={`/app/${app.route}`} className='btn btn-primary float-end'>
                        Open App
                    </NavLink>
                </CustomCard>
            </Col>
        ))}
    </Row>
    </>
  );
}

export default MyApps;