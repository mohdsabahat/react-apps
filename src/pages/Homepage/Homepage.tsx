import React from 'react'
import { useNavigate } from 'react-router-dom';
import { APP_URLS } from '../../utils/constants';

const Homepage: React.FC = () => {
    const navigate = useNavigate();
    return (
        <div className='text-center'>
            <h1 className='display-5'>Welcome!</h1>
            <p className="lead">This is a collection of React apps created by me.</p>
            <div className='d-flex justify-content-center'>
                <button 
                    onClick={() => navigate(APP_URLS.myApps.path)}
                    className='btn btn-primary'>Click Here to explore apps <span className='fas fa-compass'></span></button>
            </div>
        </div>
    )
}

export default Homepage;