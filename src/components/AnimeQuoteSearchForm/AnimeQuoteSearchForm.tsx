import React from 'react';

import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import toast from 'react-hot-toast';

import { Config } from '../../config';
import { AnimeQuote } from '../AnimeQuotes/AnimeQuotes';

interface AnimeQuoteSearchFormProps {
    setQuotes: React.Dispatch<React.SetStateAction<AnimeQuote[]>>;
    setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
}

const AnimeQuoteSearchForm: React.FC<AnimeQuoteSearchFormProps> = ({setQuotes, setIsLoading}) => {

    const [searchIn, setSearchIn] = React.useState<string>('');
    const [searchQuery, setSearchQuery] = React.useState<string>('');

    const handleFormSubmit = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        console.log(searchIn, searchQuery);
        if(searchQuery === '' || searchIn === '') {
            toast.error('Please fill all the fields.');
            return;
        }

        const searchQueryData = {
            search_in: searchIn,
            query: searchQuery
        }
        const searchQuotes = async () => {
            try {
                setIsLoading(true);
                const response = fetch(`${Config.ANIME_QUOTES_API}/search?query=${encodeURIComponent(searchQueryData.query)}&search_in=${searchQueryData.search_in}`);
                response
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    setQuotes(data.quotes);
                })
                .catch(err => {
                    console.log(err);
                    toast.error('Something went wrong');
                    setQuotes([]);
                })
                .finally(() => {
                    setIsLoading(false);
                });
            } catch (error) {
                console.log(error);
            }
        }
        searchQuotes();
    }

    const handleRandomQuoteClick = () => {
        const getRandomQuote = () => {
        
            setIsLoading(true);
            const response = fetch(`${Config.ANIME_QUOTES_API}/random`);
            response
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if(data?.quote)
                    setQuotes([data.quote]);
                else
                    setQuotes([]);
            })
            .catch(err => {
                console.log(err);
                toast.error('Something went wrong');
                setQuotes([]);
            })
            .finally(() => {
                setIsLoading(false);
            });
        }
        getRandomQuote();
    }

    return (
        <Col sm={12} xs={12} md={6} lg={4}>
            <Form>
                <Form.Group>
                    <Form.Label>Search In</Form.Label>
                    <Form.Select 
                        onChange={(e) => setSearchIn(e.target.value)}
                    >
                        <option hidden selected>-- Where to search --</option>
                        <option value='text'>Quote</option>
                        <option value='anime'>Anime Name</option>
                        <option value='author'>Author Name</option>
                    </Form.Select>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Search</Form.Label>
                    <Form.Control type='text' 
                        placeholder='Search anime quotes' 
                        name="searchQuery"
                        onChange={(e) => setSearchQuery(e.target.value)}
                    />
                    <button 
                        type="submit" 
                        className='btn btn-primary mt-2'
                        onClick={handleFormSubmit}
                    >Search</button>
                </Form.Group>
            </Form>
            <hr/>
            <button 
                className='btn btn-primary mx-auto'
                onClick={handleRandomQuoteClick}
            >Random Quote <span className='fas fa-random'></span></button>
        </Col>
    )
}

export default AnimeQuoteSearchForm;