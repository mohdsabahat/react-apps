import React, { useState } from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import AnimeQuoteSearchForm from '../AnimeQuoteSearchForm/AnimeQuoteSearchForm';
import Card from 'react-bootstrap/Card';
import Loader from '../Loader/Loader';

export type AnimeQuote = {
  anime: string;
  author: string;
  text: string;
  id: number;
}

const AnimeQuotes: React.FC = () => {

  const [quotes, setQuotes] = useState<AnimeQuote[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const noOfQuotes = quotes.length;

    return (
      <>
      <h1>Anime Quotes</h1>
      <Row>
        <AnimeQuoteSearchForm 
          setQuotes={setQuotes}
          setIsLoading={setIsLoading}
        />
        <Col xs={12} sm={12} md={6} lg={8}>
          <div>
            <h2 className='text-center'>Quotes ({noOfQuotes} results)</h2>
            <div style={{overflowY: 'auto', maxHeight: '65vh'}}>
              {isLoading && <Loader text='Loading...'/>}
              {quotes && quotes.map((quote) => {
                return (
                  <Card key={quote.id}>
                    <Card.Body>
                      <Card.Text>{quote.text}</Card.Text>
                      <i>{quote.author}</i> - <b>{quote.anime}</b>
                    </Card.Body>
                  </Card>
                )
              })}
            </div>
          </div>
        </Col>
      </Row>
      </>
    )
}

export default AnimeQuotes;