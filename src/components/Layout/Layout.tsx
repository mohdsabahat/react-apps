import React, { ReactNode } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Navigation from '../NavigationBar/NavigationBar';
import Container from 'react-bootstrap/Container';

import { useLocation } from 'react-router-dom';


type LayoutProps = {
    children: ReactNode;
};

const Layout: React.FC<LayoutProps> = ({ children }) => {

    const location = useLocation();
    const hideNavbarFor = ['/login', '/signup'];    // TODO: add these in a config file
    const showNavbar = !hideNavbarFor.includes(location.pathname);
    const thisYear = new Date().getFullYear()
    return (
        <div className="d-flex flex-column min-vh-100">

            {showNavbar && <Navigation />}

            <Container fluid={true} className={`${!showNavbar ? 'my-auto' : 'navbar-offset'}`} style={{height: '100vh'}}>
                {children}
            </Container>

            <footer className="bg-dark text-center text-white fixed-bottom" style={{zIndex: 1000}}>
                <Container className="py-2">
                    <Row>
                        <Col className="col-lg-12 d-flex align-items-center justify-content-center">
                            <p style={{color: 'white'}} className="m-0 company-name">© <span id="year">{thisYear}</span> React Apps</p>
                        </Col>
                    </Row>
                </Container>
            </footer>
        </div>
    );
};

export default Layout;