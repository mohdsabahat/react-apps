import React from 'react'
import { Card, CardProps } from 'react-bootstrap';

import './CustomCard.css';

interface CustomCardProps extends CardProps{
  cardText: string;
  effects?: boolean;
}

const CustomCard: React.FC<CustomCardProps> = ({ title, cardText, children, effects }) => {
  return (
    <Card className={`${effects ? 'customCard' : ''}`}>
        <Card.Body>
            <Card.Title>{title}</Card.Title>
            <Card.Text>
                {cardText}
            </Card.Text>
            {children}
        </Card.Body>
    </Card>
  )
}

export default CustomCard;