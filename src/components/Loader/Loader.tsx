import React from 'react'
import { Spinner } from 'react-bootstrap';

interface LoaderProps {
    text: string;
}

const Loader: React.FC<LoaderProps> = ({ text }) => {
    return (
        <div className="d-flex justify-content-center align-items-center">
            <Spinner animation="border" role="status">
                <span className="sr-only">{text}</span>
            </Spinner>
    </div>
    )
}

export default Loader;