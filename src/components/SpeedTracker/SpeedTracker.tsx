import React, { useState } from 'react';
import useGeolocation from '../../hooks/useGeoLocation';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';

const defaultMapProps = {
    center: {
      lat: 19.1166191,
      lng: 72.9374227
    },
    zoom: 11
  };

  const containerStyle = {
    width: '400px',
    height: '400px'
  };

const SpeedTracker: React.FC = () => {

    const { state: {location, error, speed}, startTracking, stopTracking } = useGeolocation();
    
    const [isTracking, setIsTracking] = useState(false);

    const handleStartTracking = () => {
        startTracking();
        setIsTracking(true);
    }
    const handleStopTracking = () => {
        stopTracking();
        setIsTracking(false);
    }
    
    return (
    <div className='h-100'>
        <div>
            {!isTracking && <button className='btn btn-primary' onClick={handleStartTracking}>Start Tracking <span className='fas fa-play'></span></button>}
            {isTracking && <button className='btn btn-danger' onClick={handleStopTracking}>Stop Tracking <span className='fas fa-stop'></span></button>}
            {error && <div>{error.message}</div>}
            <div>Location : {location && <span>{location.coords?.latitude} : {location.coords?.longitude}</span>}</div>
            <p className='lead'>Your Speed: <span className='speed'>{speed}</span></p>
        </div>
        <div className='h-100 w-100'> 
        {location && <MapContainer style={{height: '400px'}} center={[location.coords?.latitude, location.coords?.longitude]} zoom={13} scrollWheelZoom={false}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={[location.coords?.latitude, location.coords?.longitude]}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
        </MapContainer>}
        </div>
    </div>
  )
}

export default SpeedTracker;