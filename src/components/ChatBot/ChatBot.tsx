import React, { ChangeEvent, useEffect, useRef, useState } from 'react'
import { Form, FormSelect, InputGroup } from 'react-bootstrap';

import './ChatBot.css';
import toast from 'react-hot-toast';
import { ChatBotPersona, Config } from '../../config';

enum Sender {
    Bot = "bot",
    User = "user",
}
enum BotStatus {
    Offline = "offline",
    Online = "online",
    Typing = "typing"
}
interface Message {
    name: Sender;
    text: string;
    timestamp: number;
}

const freeMemes = [
	'Dude, you smashed my turtle saying "I\'M MARIO BROS!"',
	'Dude, you grabed seven oranges and yelled "I GOT THE DRAGON BALLS!"',
	'Dude, you threw my hamster across the room and said "PIKACHU I CHOOSE YOU!"',
	'Dude, you congratulated a potato for getting a part in Toy Story',
	'Dude, you were hugging an old man with a beard screaming "DUMBLEDORE YOU\'RE ALIVE!"',
	'Dude, you were cutting all my pinapples yelling "SPONGEBOB! I KNOW YOU\'RE THERE!"',
];

export interface Persona {
    id: number,
    name: string,
    show?: string,
    show_type?: string,
    avatar: string,
    medium_link?: string,
    full_link?: string
}

const timestampToTime = (timestamp: number) => {
    const date = new Date(timestamp);
    return date.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
};

const errMessage = {name: Sender.Bot, text: 'An Error occurred while fetching a response.'};
const defaultPersona: Persona | undefined = ChatBotPersona.find((persona) => persona.name.toLowerCase() === 'simsimi');

const ChatBot: React.FC = () => {

    const [messages, setMessages] = useState<Message[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [botStatus, setBotStatus] = useState<string>(BotStatus.Offline);
    const [persona, setPersona] = useState<Persona | undefined>(defaultPersona);
    const [sessionId, setSessionId] = useState<string|null>(null);

    const messageContainerRef = useRef<HTMLDivElement>(null);
    const textInputRef = useRef<HTMLInputElement>(null);
    const chatPersonaRef = useRef<HTMLSelectElement>(null);

    const getGeminiResponse = async (data : any) => {
        const URL = 'https://random-flask-api.onrender.com/api/v1/gemini/chat';
        const resp = await fetch(URL, {
            method: 'POST',
            headers: {
                'X-API-KEY': Config.GEMINI_API_KEY,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        if (!resp.ok) {
            throw new Error('Failed to get response from the bot');
        }
        const botResp = await resp.json();
        if(botResp.error){  throw new Error("Error response from API!"); }
        const sessId = botResp.session_id;
        const text = botResp.bot_response;
        return {sessId, text};
    }

    const getSimsimiResponse = async (data : any) => {
        const URL = 'https://random-flask-api.onrender.com/api/v1/simsimi/chat';
        const resp = await fetch(URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: data
        });
        if (!resp.ok) {
            throw new Error('Failed to get response from the bot');
        }
        const botResp = await resp.json();
        if(botResp.error){  throw new Error("Error response from API!"); }
        return botResp.message;
    }

    const handleMessageSend = async () => {
        const userQuery: string = textInputRef?.current?.value ?? '';
        if(userQuery === '') {
            toast.error('Please enter a message!');
            return;
        }
        if(textInputRef.current) textInputRef.current.value = '';
        
        const userNewMessage: Message = {
            name: Sender.User, text: userQuery, timestamp: Date.now()
        };
        setMessages(prevMessages => [...prevMessages, userNewMessage]);

        setIsLoading(true);
        try{
            if(persona?.name.toLowerCase() === 'simsimi'){
                const response = await getSimsimiResponse(new URLSearchParams({
                    lc: 'en',
                    text: userQuery
                }));
                const newMessage = {
                    name: Sender.Bot,
                    text: response,
                    timestamp: Date.now()
                };
                setSessionId(null);
                // Use the functional form of setMessages to ensure state consistency
                setMessages(prevMessages => [...prevMessages, newMessage]);
            } else {
                const data = {
                    message: userQuery,
                    session_id: sessionId,
                    character_name: persona?.name,
                    character_show: persona?.show,
                    show_type: persona?.show_type
                };
                const {sessId, text} = await getGeminiResponse(data);
                const newMessage = {
                    name: Sender.Bot,
                    text: text,
                    timestamp: Date.now()
                };
                setSessionId(sessId);
                // Use the functional form of setMessages to ensure state consistency
                setMessages(prevMessages => [...prevMessages, newMessage]);
            }
        } catch(err) {
            console.log('Error: ', err);
            // Bot responds about the error
            setMessages(prevMessages => [...prevMessages, {...errMessage, timestamp: Date.now()}]);
        } finally {
            if (textInputRef.current){
                textInputRef.current.focus();
            }
            setIsLoading(false);
        }
    };

    const handleInputEnterPress = async (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Check if the Enter key is pressed (key code 13)
        if (event.key === 'Enter') {
            // Prevent the default behavior of form submission
            event.preventDefault();
            // Send the message or perform any action
            await handleMessageSend();
        }
    }

    const onPersonaChange = (e: ChangeEvent<HTMLSelectElement>) => {
        const isConfirm = window.confirm("Are you sure?\nThis will clear your current chat.");
        if(isConfirm){
            setPersona(ChatBotPersona.find( persona => persona.id === Number(e.target.value)))
            setMessages([]);
            setSessionId(null);
        } else {
            if(chatPersonaRef.current){
                const curr = chatPersonaRef.current;
                curr.value = persona?.id.toString() ?? '';
            }
        }
    }

    useEffect(() => {
        // Scroll to the bottom of the message container whenever messages change
        if (messageContainerRef.current) {
            messageContainerRef.current.scrollTop = messageContainerRef.current.scrollHeight;
        }
    }, [messages]);

    return (
        <div>
            <div className='d-flex flex-column border' style={{height: 'calc(100vh - 102px)'}}>
                {/* Header */}
                <div className='border-bottom'>
                    <div className='d-flex px-3 py-2 align-items-center'>
                        <div className="position-relative">
                            <img 
                                src={persona?.avatar}
                                // src="https://bootdey.com/img/Content/avatar/avatar3.png" 
                                className="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"
                            ></img>
                        </div>
                        <div className="flex-grow-1 ps-3">
                            {/* <strong>Simsimi Bot</strong> */}
                            <FormSelect 
                                className='form-select-sm'
                                style={{width: 'auto'}} 
                                onChange={onPersonaChange}
                                defaultValue={defaultPersona?.id}
                                ref={chatPersonaRef}
                            >
                                {ChatBotPersona.map((persona) => {
                                    return <option value={persona.id}>{persona.name}</option>
                                })}
                            </FormSelect>
                            {/* <div className="text-muted small"><em>Typing...</em></div> */}
                            <div className='text-muted small'>
                                <em>{isLoading? BotStatus.Typing : botStatus}</em>
                            </div>
                        </div>
                        {/* <div>
                            Right most items
                        </div> */}
                    </div>
                </div>
                {/* Content */}
                <div className='position-relative h-100' style={{maxHeight: 'calc(100% - 120px)'}}>
                    <div className='messages d-flex flex-column p-4 h-100' 
                        ref={messageContainerRef}
                        style={{background: `#efe7dd url("https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png") repeat`}}
                    >
                        {messages && messages.map( message => {
                            return(
                                <div className={`${message.name === Sender.Bot ? 'chat-message-left' : 'chat-message-right'} pb-2`}>
                                    <div>
                                        <img src={`${persona?.avatar}`} className="rounded-circle mr-1" alt="Chris Wood" width="40" height="40"></img>
                                        <div className="text-dark small text-nowrap mt-2">{timestampToTime(message.timestamp)}</div>
                                    </div>
                                    <div className={`message-text flex-shrink-1  py-2 px-3 ${message.name === Sender.Bot ? 'ms-3' : 'me-3'} position-relative`}>
                                        <div className="font-weight-bold mb-1">{message.name}</div>
                                        {message.text}
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                {/* message bar */}
                <div className='message-bar px-3 py-2 border-top'>
                    <InputGroup>
                        <Form.Control
                            placeholder="Say 'hi'"
                            ref={textInputRef}
                            onKeyDown={handleInputEnterPress}
                        />
                        <button 
                            className='btn btn-primary'
                            onClick={handleMessageSend}
                            disabled={isLoading}
                        >
                            Send
                            <span className='ms-2 fas fa-paper-plane'></span>
                        </button>
                    </InputGroup>
                </div>
            </div>
        </div>
    )
}

export default ChatBot;