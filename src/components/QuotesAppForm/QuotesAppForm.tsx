import React from 'react'
import { Button, Form } from 'react-bootstrap';

interface QuotesAppFormProps {
    searchTerm: string;
    setSearchTerm: React.Dispatch<React.SetStateAction<string>>;
    searchIn: string;
    setSearchIn: React.Dispatch<React.SetStateAction<string>>;
    handleSearch: (e: React.FormEvent) => void;
    getRandomQuote: () => void;
    isLoading: boolean;
    error: string;
}

const QuotesAppForm: React.FC<QuotesAppFormProps> = ({searchTerm, setSearchTerm, searchIn, setSearchIn, isLoading, handleSearch, getRandomQuote }) => {
    return (
        <Form onSubmit={handleSearch}>
            <Form.Group>
              <Form.Label>Search in ?</Form.Label>
              <Form.Select required onChange={(e) => setSearchIn(e.target.value)} defaultValue={searchIn}>
                <option hidden value=''>-- Where to search --</option>
                <option value='quote'>Quote</option>
                <option value='author'>Author</option>
                <option value='tags'>Tags</option>
              </Form.Select>
            </Form.Group>
            <Form.Group controlId="searchForm">
              <Form.Label>Search for Quote:</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter search term"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
              />
            </Form.Group>
            <Button 
              variant="primary" 
              type='submit'
              disabled={isLoading}
              className='w-100 mt-2'
            >
              Search
            </Button>
            <hr />
            <Button 
              variant="primary" 
              className='w-100'
              onClick={getRandomQuote}
              disabled={isLoading}
            >
              Random Quote <span className='fas fa-random'></span>
            </Button>
        </Form>
    )
}

export default QuotesAppForm;