import React, { useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import toast from 'react-hot-toast';

import { Config } from '../../config';
import Loader from '../Loader/Loader';

import './QuotesApp.css';
import QuotesAppForm from '../QuotesAppForm/QuotesAppForm';

type Quote = {
  id: number;
  quote: string;
  author: string;
  tags: string;
}

const highlightSubstring = (text: string, substring: string, highlightClassName: string = 'highlight') => {
  if (substring === '') return text;
  const parts = text.split(new RegExp(`(${substring})`, 'gi'));
  return parts.map((part, index) => 
    part.toLowerCase() === substring.toLowerCase() ? 
    <span key={index} className={highlightClassName}>{part}</span> : part
  );
};

const QuotesApp: React.FC = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchIn, setSearchIn] = useState('');
  const [searchResults, setSearchResults] = useState<Quote[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [currentUtterance, setCurrentUtterance] = useState<SpeechSynthesisUtterance | null>(null);

  const noOfQuotes = searchResults.length;
  const synth = window.speechSynthesis;

  const handleSearch = (e: React.FormEvent) => {
    e.preventDefault();
    if (searchTerm === '' || searchIn === '') {
      toast.error('Please enter a search term and select where to search');
      return;
    }
    const searchQuotes = () => {
      try {
        setIsLoading(true);
        setSearchResults([]);
        const response = fetch(`${Config.QUOTES_API}/search?query=${searchTerm}&search_in=${searchIn}`);
        response
          .then(res => res.json())
          .then(data => {
            console.log(data);
            if(data.quotes && data.quotes.length > 0) {
              setSearchResults(data.quotes);
              setError('');
            } else {
              setError('No quotes found');
              toast.error('No quotes found');
            }
          })
          .catch(err => {
            console.log(err);
            setError('Something went wrong');
            toast.error('Something went wrong');
            setSearchResults([]);
          })
          .finally(() => {
            setIsLoading(false);
          });
      } catch (error) {
        console.log(error);
      }
    }

    searchQuotes();
  };

  const getRandomQuote = () => {
    try {
      setIsLoading(true);
      setSearchResults([]);
      setSearchTerm('');
      const response = fetch(`${Config.QUOTES_API}/random`);
      response
        .then(res => res.json())
        .then(data => {
          console.log(data);
          if(data.quote) {
            setSearchResults([data.quote]);
            setError('');
          } else {
            setError('No quotes found');
            toast.error('No quotes found');
          }
        })
        .catch(err => {
          console.log(err);
          setError('Something went wrong');
          toast.error('Something went wrong');
          setSearchResults([]);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } catch (error) {
      console.log(error);
    }
  }


  const speakText = (text: string) => {
    if (synth.speaking && currentUtterance) {
      synth.cancel();
      return;
    }

    const utterance = new SpeechSynthesisUtterance(text);
    synth.speak(utterance);
    setCurrentUtterance(utterance);
  };

  return (
    <Container>
      <Row className='gy-3'>
        <h2>Quotes App</h2>
        <Col xs={12} sm={12} md={4} lg={3}>
          <QuotesAppForm
            searchIn={searchIn}
            setSearchIn={setSearchIn}
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
            handleSearch={handleSearch}
            getRandomQuote={getRandomQuote}
            isLoading={isLoading}
            error={error}
          />
        </Col>
        <Col xs={12} sm={12} md={8} lg={9}>
          <h3 className='text-center mb-2'>Quotes ({isLoading ? 'Loading...' : `${noOfQuotes} Results`})</h3>
          {error && <div>{error}</div>}
          {isLoading && <Loader text="Loading..."/>}
          <div style={{overflowY: 'auto', maxHeight: '65vh'}}>
            {searchResults.map((quote, index) => (
              <Card key={quote.id} className='mb-2'>
                <Card.Body>
                  <span 
                    className='fas fa-volume-up float-end' 
                    role='button'
                    onClick={() => speakText(quote.quote)}
                    title='Click to read the quote aloud'
                    ></span>
                  <blockquote>{highlightSubstring(quote.quote, searchTerm)}</blockquote>
                </Card.Body>
                <Card.Footer className='d-flex'>
                  <span>{quote.author}</span>
                  <div className='ms-auto'>
                    {quote.tags.split('|').map( tag => <span className='badge bg-primary mx-1' key={tag}>{tag}</span>)}
                  </div>
                </Card.Footer>
              </Card>
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default QuotesApp;