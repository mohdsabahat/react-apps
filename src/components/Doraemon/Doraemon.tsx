import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import DoraemonSearchForm from '../DoraemonSearchForm/DoraemonSearchForm';
import GadgetList from '../GadgetList/GadgetList';
import Loader from '../Loader/Loader';

export interface Gadget {
    id: number;
    name: string;
    type: string;
    description: string;
    imageUrl: string;
    function: string;
}

const Doraemon: React.FC = () => {
    const [gadgets, setGadgets] = useState<Gadget[]>([]);
    const [isGadgetLoading, setIsGadgetLoading] = useState<boolean>(false);
    const [gadgetError, setGadgetError] = useState<string>('');

    return (
        <>
        <h1 className='text-center mb-3'>Doraemon Gadgets</h1>
        <Row className='gy-3'>
            <DoraemonSearchForm 
                setGadgets={setGadgets}
                isGadgetLoading={isGadgetLoading}
                setIsGadgetLoading={setIsGadgetLoading}
                setGadgetError={setGadgetError}
            />
            <Col xs={12} sm={12} md={6} lg={6}>
                <main>
                    {isGadgetLoading && <Loader text="Loading..."/>}
                    {gadgetError.length>0 && <p>{gadgetError}</p>}
                    {!isGadgetLoading && !(gadgetError.length>0) && 
                    <>
                        <h2 className='text-center'>Search results ({gadgets?.length} results)</h2>
                        <GadgetList gadgets={gadgets} />
                    </>
                    }
                </main>
            </Col>
        </Row>
        </>
    );
}

export default Doraemon;