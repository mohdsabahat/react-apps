import React from 'react'
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';

import toast from 'react-hot-toast';
import { Config } from '../../config';
import { Gadget } from '../Doraemon/Doraemon';

interface DoraemonSearchFormProps {
    setGadgets: React.Dispatch<React.SetStateAction<Gadget[]>>;
    isGadgetLoading: boolean;
    setIsGadgetLoading: React.Dispatch<React.SetStateAction<boolean>>;
    setGadgetError: React.Dispatch<React.SetStateAction<string>>;
}

const GADGET_SEARCH_ENDPOINT = '/gadgets/search';

const DoraemonSearchForm: React.FC<DoraemonSearchFormProps> = ({ setGadgets, isGadgetLoading, setIsGadgetLoading, setGadgetError } : DoraemonSearchFormProps) => {
    const [searchQuery, setSearchQuery] = React.useState<string>('');

    const handleGadgetSearch = (e:React.FormEvent) => {
        e.preventDefault();
        console.log('Searching for gadget:', searchQuery);
        if(searchQuery === '') {
            toast.error('Please enter a gadget name to search');
            return;
        }
        searchGadget();
        function searchGadget() {
            setIsGadgetLoading(true);
            setGadgetError('');
            const resp = fetch(`${Config.DORAEMON_GADGET_API}${GADGET_SEARCH_ENDPOINT}?query=${searchQuery}`);
            resp.then((data) => data.json() )
            .then((data) => {
                console.log(data)
                if(data.gadgets &&  data.gadgets.length > 0) {
                    setGadgets(data.gadgets);
                } else {
                    toast.error('No gadgets found for the search query');
                    setGadgets([]);
                }
            })
            .catch((error) => {
                console.error('Error fetching gadgets:', error);
                setGadgetError('Error fetching gadgets. Please try again later.');
                toast.error('Error fetching gadgets. Please try again later.');
                setGadgets([]);
            })
            .finally(() => {
                setIsGadgetLoading(false);
            });
        }
    }

  return (
    <Col xs={12} sm={12} md={6} lg={6}>
        <aside className='position-static'>
            <Card>
                <Card.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="formGadget">
                            <Form.Label>Enter the gadget name</Form.Label>
                            <Form.Control type="text" placeholder="Enter the gadget name" 
                                autoFocus
                                value={searchQuery}
                                onChange={(e) => setSearchQuery(e.target.value)}
                            />
                        </Form.Group>
                        <button type="submit" className="btn btn-primary" 
                            onClick={handleGadgetSearch}
                            disabled={isGadgetLoading}
                        >Search Gadget</button>
                    </Form>
                </Card.Body>
            </Card>
        </aside>
    </Col>
  )
}

export default DoraemonSearchForm;