import { useEffect, useState } from "react";

import LocalStorageUtils from "../../utils/localstorage";

import './ThemeSwitch.css';

const ThemeSwitch = () => {
    const DEFAULT_THEME = "dark";
    const [theme, setTheme] = useState(`${DEFAULT_THEME}`);


    useEffect(() => {
        const localTheme = LocalStorageUtils.get("theme");
        const isValid = localTheme === "light" || localTheme === "dark";
        isValid && setTheme(localTheme);
    }, []);

    useEffect(() => {
        document.documentElement.setAttribute('data-bs-theme', theme);
    }, [theme]);

    const changeTheme = (themeColor: string) => {
        // Change the theme to specified value
        const isValid = themeColor === "light" || themeColor === "dark";
        if(isValid) {
            // save to local storage
            LocalStorageUtils.save("theme", themeColor, 0);
        }
        setTheme(isValid ? themeColor : DEFAULT_THEME);
    }

    return (
            <div className="theme-switch-container">
                <div className="theme-switch">
                    <label htmlFor="theme" className="theme">
                        <span className="theme__toggle-wrap">
                            <input 
                                id="theme" className="theme__toggle" 
                                type="checkbox" role="switch" name="theme" 
                                value="dark" checked={theme === "dark"}
                                onChange={() => changeTheme(theme === "dark" ? "light" : "dark")}
                            />
                        </span>
                    </label>
                </div>
            </div>
    )
}


export default ThemeSwitch;