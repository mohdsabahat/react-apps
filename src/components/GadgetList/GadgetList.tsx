import React from 'react'
import Card from 'react-bootstrap/Card';

import { Gadget } from '../Doraemon/Doraemon';

interface GadgetListProps {
    gadgets: Gadget[];
}

const GadgetList: React.FC<GadgetListProps> = ({ gadgets }) => {
  return (
    <div className="card-list" style={{overflowY: 'auto', maxHeight: '65vh'}}>
        {gadgets.map((gadget) => (
            <Card key={gadget.id} className='mb-2'>
                <Card.Body>
                    <Card.Title>{gadget.name}</Card.Title>
                    <div className='d-flex'>
                        <div className='w-25'>
                            <img className='img w-100' src='https://placehold.co/200' alt={gadget.name}></img>
                        </div>
                        <Card.Text className='w-100 ms-2'>{gadget.description ?? 'No description'}</Card.Text>
                    </div>
                </Card.Body>
            </Card>
        ))}
    </div>
  )
}

export default GadgetList;