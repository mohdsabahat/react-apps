import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import ThemeSwitch from '../ThemeSwitch/ThemeSwitch';
import { Link, useLocation } from 'react-router-dom';

import { APP_URLS } from '../../utils/constants';
import { Config } from '../../config';

import './NavigationBar.css';


const NavigationBar: React.FC = () => {
    const unprotectedRoutes:string[] = [];
    const location = useLocation();
    let showProtectedRoutes = true;
    
    if (unprotectedRoutes.includes(location.pathname)) {
        showProtectedRoutes = false;
    }
    return (
        <Navbar expand="lg" fixed='top'
            collapseOnSelect={true}
        >
            <Navbar.Brand as={Link} to={APP_URLS.home.path}>{Config.appName}</Navbar.Brand>
            <Navbar.Toggle>
                <span className="fas fa-bars"></span>
            </Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
                {showProtectedRoutes ? 
                    (<Nav className="mx-auto pe-2">
                        <Nav.Link as={Link} to={`${APP_URLS.myApps.path}`} eventKey={APP_URLS.myApps.path}>
                            <span className="fab fa-app-store"></span> {APP_URLS.myApps.name}
                        </Nav.Link>
                        {/* <Nav.Link href={`${APP_URLS.profile}`}>
                            <span className="fas fa-user"></span> Profile
                        </Nav.Link> */}
                    </Nav>)
                    :
                    (
                        <Nav className="mx-auto pe-2">
                            {/* <Nav.Link href={`${APP_URLS.login}`}>
                                <span className="fas fa-user"></span> Login
                            </Nav.Link>
                            <Nav.Link href={`${APP_URLS.signup}`}>
                                <span className="fas fa-users"></span> Signup
                            </Nav.Link> */}
                        </Nav>
                    )
                }
                <Nav>
                    <ThemeSwitch />
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default NavigationBar;