import React from 'react';
import { BrowserRouter , Routes, Route } from 'react-router-dom';
import './App.css';
import { Toaster } from 'react-hot-toast';

import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from './components/Layout/Layout';
import MyApps from './pages/MyApps/MyApps';

import { APP_URLS } from './utils/constants';
import SpeedTracker from './components/SpeedTracker/SpeedTracker';
import Doraemon from './components/Doraemon/Doraemon';
import AnimeQuotes from './components/AnimeQuotes/AnimeQuotes';
import QuotesApp from './components/QuotesApp/QuotesApp';
import AppLayout from './components/AppLayout/AppLayout';
import Homepage from './pages/Homepage/Homepage';
import ChatBot from './components/ChatBot/ChatBot';

function App() {
    return (
        <BrowserRouter>
            <Toaster position='top-right' />
            <Layout>
                <Routes>
                    <Route path={APP_URLS.home.path} element={<Homepage />} />
                    <Route path={APP_URLS.myApps.path} element={<MyApps />} />
                    <Route path='/app' element={<AppLayout />}>
                        <Route path='doraemon' element={<Doraemon />} />
                        <Route path='anime-quotes' element={<AnimeQuotes />} />
                        <Route path='quotes-app' element={<QuotesApp />} />
                        <Route path='speed-tracker' element={<SpeedTracker />} />
                        <Route path='chatbot' element={<ChatBot />} />
                    </Route>
                    <Route path='*' element={<h1>Page not found</h1>} />
                </Routes>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
