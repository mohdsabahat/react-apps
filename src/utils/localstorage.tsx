type LocalStorageData<T> = {
    value: T;
    expiration?: number;    // expiration time in milliseconds, 0 is infinite
    createdAt: number;
}

class LocalStorageUtils {
    static DEFAULT_EXPIRATION_TIME = 0; // infinite time by default

    static save<T>(key: string, value: T, 
        expirationTime: number = LocalStorageUtils.DEFAULT_EXPIRATION_TIME): void {
        const data: LocalStorageData<T> = { 
            value,
            createdAt: new Date().getTime()
        };
        if(expirationTime > 0){
            data.expiration = expirationTime;
        }
        localStorage.setItem(key, JSON.stringify(data) );
    }

    static get<T>(key: string): T | null {
        const item = localStorage.getItem(key);
        if(item) {
            try {
                const data: LocalStorageData<T> = JSON.parse(item);

                // check if value is a string (possible double stringified)
                // https://stackoverflow.com/questions/42494823
                if(typeof data.value === 'string') {
                    // attempt to parse again
                    try {
                        data.value = JSON.parse(data.value);
                    } catch (err ){
                        console.debug("Error while double parsing the localstorage item.", err);
                    }
                }

                // check expiration 
                if(data.expiration && new Date().getTime() - data.createdAt > data.expiration) {
                    // expired
                    localStorage.removeItem(key);
                    return null;
                }
                return data.value;
            } catch (error) {
                console.error("Error parsing localkstorage item : ", error);
                return null;
            }
        }
        return null;
    }
}

export default LocalStorageUtils;