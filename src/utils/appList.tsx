
export const myAppList = [
    {
        name: 'Doraemon Gadgets',
        description: 'Search and browse through all recorded collection of gadgets used by Doraemon in Manga or Anime.',
        route: 'doraemon'
    },
    {
        name: 'Anime Quotes',
        description: 'Search and browse through all recorded collection of quotes from various anime characters.',
        route: 'anime-quotes'
    },
    {
        name: 'Quotes App',
        description: 'A collection of quotes from various authors.',
        route: 'quotes-app'
    },
    {
        name: 'Speed Tracker',
        description: 'Track your speed in real-time, using the geolocation API. Also can trace your path on the map.',
        route: 'speed-tracker'
    },
    {
        name: 'Chatbot',
        description: 'Conversational chatbot which utilizes the simsimi chatbot api (warning: May produce racial and abusive results)',
        route: 'chatbot'
    }
];
