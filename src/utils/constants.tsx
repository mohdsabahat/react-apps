
// URL constants
export const APP_URLS = {
    home : {path: '/', name: 'Home'},
    myApps : {path: '/apps', name: 'My Apps'},
};