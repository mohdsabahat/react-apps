import { Persona } from "./components/ChatBot/ChatBot"


export const Config = {
    appName: 'React Apps',
    DORAEMON_GADGET_API: 'https://random-flask-api.onrender.com/api/v1/doraemon',
    ANIME_QUOTES_API: 'https://random-flask-api.onrender.com/api/v1/anime/quotes',
    QUOTES_API: 'https://random-flask-api.onrender.com/api/v1/quotes',
    GEMINI_API_KEY: 'c9e2d855-84f3-4b5a-941a-32a79ffb19a7'
}

export const ChatBotPersona : Persona[] = [
    {
        id: 1,
        name: 'Simsimi',
        avatar: 'https://avatars.dicebear.com/api/avataaars/simsimi.svg',
    },
    {
        id: 2,
        name: 'Naruto',
        show: 'Naruto',
        show_type: 'Anime',
        avatar: 'https://i.ibb.co/bv4rQKD/image.png',
        full_link: 'https://i.ibb.co/8xyjN7t/image.png',
        medium_link: 'https://i.ibb.co/wQnKBzj/image.png'
    },
    {
        id: 3,
        name: 'Goku',
        show: 'Dragon Ball Z',
        show_type: 'Anime',
        avatar: 'https://i.ibb.co/wc5j7Bf/image.png',
        full_link: 'https://i.ibb.co/5KbwkGP/image.png',
        medium_link: 'https://i.ibb.co/kmNvJ5t/image.png'
    },
    {
        id: 4,
        name: 'Tony Stark',
        show: 'Iron man',
        show_type: 'Movie',
        avatar: 'https://i.ibb.co/jD66fTh/image.png',
        full_link: 'https://i.ibb.co/1TvvQLZ/image.png',
        medium_link: 'https://i.ibb.co/3crrd71/image.png'
    }
]